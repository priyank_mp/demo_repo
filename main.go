package main

import (
	"fmt"
	"reflect"
)

type A struct {
	a1 int
	a2 string
}

type B struct {
	A
	b1 string
}

type C struct {
	A
	c1 bool
}

func isTypeOf(v interface{}, targetType interface{}) bool {
	fmt.Printf("\nv: %T, %v\n", v, v)
	fmt.Printf("targetType: %T, %v\n", targetType, targetType)

	targetTypeValue := reflect.ValueOf(targetType)
	result := reflect.TypeOf(v) == targetTypeValue.Type()
	fmt.Printf("%v\n\n", result)
	return result
}

func changeIt(bcp B) {
	fmt.Println("changeIt")
	bcp.b1 = "new val"
}

func changeItPtr(bcp *B) {
	fmt.Println("changeItPtr")
	bcp.b1 = "new val ptr"
}

func main() {
	a := A {
		a1: 1,
		a2: "priyank",
	}
	b := B {
		A: a,
		b1: "b1 string",
	}

	fmt.Println(a)
	fmt.Println(b)

	changeIt(b)

	fmt.Println(a)
	fmt.Println(b)

	changeItPtr(&b)

	fmt.Println(a)
	fmt.Println(b)

}
